/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    hello: function(req, res){
        console.log('hello world שלום עולם');
        res.json([ 1,2,3 , {hello: true, goodbye: 'later'}]);
    },
    say: function(req, res){
        var msg = req.param('message'), x= req.param('x');
        console.log('new action testing', msg, x);
        
        res.json([
            req.param('x'),
            req.param('y'),
            req.param('message'),
            {
                hello: false,
                goodbye: 'later---!'
            }
        ]);
    }
};

