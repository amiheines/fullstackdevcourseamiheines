/*global $*/
// $(function whenDomIsReady(){
//   $('.submit-video-form').submit(function (e){
//     e.preventDefault(); 
//     var newVideo = { 
//       title: $('.submit-video-form input[name="title"]').val(),	 
//       src: $('.submit-video-form input[name="src"]').val() 
//     }; 
//     $('.submit-video-form input').val('');  
//     $('.submit-video-form button').text('Submitting...'); 
//     $('.submit-video-form button').prop('disabled', true); 
//     var parser = document.createElement('a'); 
//   parser.href = newVideo.src
//   var youtubeID = parser.search.substring(parser.search.indexOf("=")+1, 
// parser.search.length); 
//     newVideo.src = 'https://www.youtube.com/embed/'+youtubeID; 
//     setTimeout(function (){ 
//       var newVideoHtml = '<li class="video">'+
//       '  <h2>' + newVideo.title + '</h2>'+ 
//       '  <iframe width="640" height="390" src="'+newVideo.src+'" frameborder="0" allowfullscreen></iframe>'+ 
//       '</li>'; 
//       $('.the-list-of-videos').prepend(newVideoHtml); 
//       $('.submit-video-form button').text('Submit Video'); 
//       $('.submit-video-form button').prop('disabled', false); 
//     }, 750); 
//   }); 
// });

angular.module('brushfire_videosPage', []) 
.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        '*://www.youtube.com/**'
    ]);
});
angular.module('brushfire_videosPage')
.controller('PageCtrl', [
     '$scope', '$timeout', '$http',
     function ( $scope ,  $timeout, $http ){
        $scope.videosLoading = true; 
        console.log('stage 1', $scope);
        $http.get('/video').then(function(res){
            console.log('GET video, res', res);
            $scope.videos = res.data;
            $scope.videosLoading = false;
        });

        $scope.submitNewVideo = function() {
            console.log('stage 3', $scope);
            if ($scope.busySubmittingVideo) {
                return;
            }       
            var _newVideo = {
                title: $scope.newVideoTitle, 
                src: $scope.newVideoSrc, 
            };      
            var parser = document.createElement('a');
            parser.href = _newVideo.src
            var youtubeID = parser.search.substring(parser.search.indexOf("=") + 1,  parser.search.length);
            _newVideo.src = 'https://www.youtube.com/embed/' + youtubeID;
            $scope.busySubmittingVideo = true;
            $http.post('/video', _newVideo).then(function(){
                console.log('added new video to server!!!!');
                $scope.videos.unshift(_newVideo);
                $scope.busySubmittingVideo = false;
                $scope.newVideoTitle = ''; 
                $scope.newVideoSrc = '';
            });
        }
    }
]);
        
        
        
        
        
